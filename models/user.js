var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");
var crypto = require("crypto");
var jwt = require("jsonwebtoken");
var jwtSecret = require("../config/settings");

var userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      unique: true,
      lowercase: true,
      required: true,
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        "Adresse mail invalide.",
      ],
    },
    pwdHash: {
      type: String,
    },
    pwdSalt: {
      type: String,
    },
  },
  { timestamps: true }
);

userSchema.plugin(uniqueValidator, { message: "déjà inscrit" });

userSchema.methods.setPassword = function (password) {
  this.pwdSalt = crypto.randomBytes(16).toString("hex");
  this.pwdHash = crypto
    .pbkdf2Sync(password, this.pwdSalt, 12000, 256, "sha512")
    .toString("hex");
};

userSchema.methods.checkPassword = function (password) {
  const pwdHash = crypto
    .pbkdf2Sync(password, this.pwdSalt, 12000, 256, "sha512")
    .toString("hex");
  return pwdHash == this.pwdHash;
};

userSchema.methods.generateToken = function () {
  return jwt.sign(
    {
      email_id: this.email,
    },
    jwtSecret,
    { expiresIn: "1d" }
  );
};

userSchema.methods.authJson = function () {
  return {
    email: this.email,
    token: this.generateToken(),
  };
};

userSchema.methods.infoJson = function () {
  return {
    email: this.email,
  };
};

mongoose.model("user", userSchema);
