var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var userModel = mongoose.model("user");
const Cookies = require("cookies");

/* GET home page. */
router.get("/", function (req, res, next) {
  var token = new Cookies(req, res).get("access_token");

  if (token) {
    res.redirect("/users");
  } else {
    res.render("login");
  }
});

router.post("/", async function (req, res, next) {
  const { email, password } = req.body;

  const existingUser = await userModel.findOne({ email: email });

  if (!(email && password)) {
    res.status(400).send("Données manquantes pour la connexion.");
  }

  if (!existingUser) {
    return res.status(409).send("Il n'y a pas de compte pour cet identifiant.");
  }

  if (!existingUser.checkPassword(password)) {
    return res.status(400).send("Mot de passe incorrect.");
  }

  const authJson = existingUser.authJson();

  new Cookies(req, res).set("access_token", authJson.token, { httpOnly: true });

  return res.redirect("/users");
});

module.exports = router;
