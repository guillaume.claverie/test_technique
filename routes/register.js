var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var userModel = mongoose.model("user");
const Cookies = require("cookies");

/* GET home page. */
router.get("/", function (req, res, next) {
  var token = new Cookies(req, res).get("access_token");

  if (token) {
    res.redirect("/users");
  } else {
    res.render("register");
  }
});

router.post("/", async function (req, res, next) {
  const { email, password } = req.body;

  if (!(email && password)) {
    return res.status(400).send("Données manquantes pour l'enregistrement.");
  }

  const userAlreadyExists = await userModel.findOne({ email: email });

  if (userAlreadyExists) {
    return res
      .status(409)
      .send("Un compte est déjà enregistré pour cet email.");
  }

  const newUser = new userModel({ email: email });
  newUser.setPassword(password);

  await newUser.save();

  return res.render("message", { message: "Compte créé avec succès." });
});

module.exports = router;
