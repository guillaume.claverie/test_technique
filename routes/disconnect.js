var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var userModel = mongoose.model("user");
const Cookies = require("cookies");

/* GET home page. */
router.get("/", function (req, res, next) {
  new Cookies(req, res).set("access_token", "", { expires: Date.now() });

  res.redirect("/login");
});

module.exports = router;
