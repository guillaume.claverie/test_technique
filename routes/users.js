var express = require("express");
var router = express.Router();
var verifyToken = require("../middleware/auth");
var mongoose = require("mongoose");
var userModel = mongoose.model("user");

/* GET users listing. */
router.get("/", verifyToken, function (req, res, next) {
  userModel.find({}, (err, users) => {
    var allUsers = users.map((value) => value.infoJson().email);
    res.render("users", {
      users: allUsers,
      disconnect: () => {
        console.log("pressed");
      },
    });
  });
});

module.exports = router;
