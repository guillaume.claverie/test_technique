const mongoose = require("mongoose");

const user = process.env.MONGO_USER;

const password = process.env.MONGO_PASSWORD;

const path = "cluster0.aftiz.mongodb.net/Cluster0";

const InitiateDatabase = async () => {
  await mongoose.connect(
    "mongodb+srv://" + user + ":" + password + "@" + path,
    (err) => {
      console.error.bind(console, "MongoDB error :" + err);
    }
  );
};

module.exports = InitiateDatabase;
