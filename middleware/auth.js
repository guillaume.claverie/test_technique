var jwt = require("jsonwebtoken");
var jwtSecret = require("../config/settings");
const Cookies = require("cookies");

var verifyToken = (req, res, next) => {
  var token = new Cookies(req, res).get("access_token");

  if (token) {
    try {
      const decoded = jwt.verify(token, jwtSecret);
      req.user = decoded;
    } catch (err) {
      new Cookies(req, res).set("access_token", "", { expires: Date.now() });
      return res.redirect("/error", { error: err });
    }
  } else {
    res.redirect("/login");
  }

  return next();
};

module.exports = verifyToken;
